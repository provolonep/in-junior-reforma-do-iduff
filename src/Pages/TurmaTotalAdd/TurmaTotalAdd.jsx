import React from 'react'
import './TurmaTotalAdd.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'

export default function Turma_Total_Add(){
    return(
        <>
        <body>
            <section className="borda-turmaadd">
                <div>
                    <BarraLateral/>
                </div>
                <div className="borda-geral-turmas-add">
                    <section className="topo-turmasadd">
                        <div>
                        <h1>Turmas</h1>
                        </div>
                    </section>
                    <section className="sub-turmasadd">
                        <div>
                            <p>Departamento:</p>
                        </div>
                    </section>
                    <form action="">
                        <section className="form-turmasadd">
                            <div>
                            <label For="Nome">Nome: </label><br/>
                            <input type="text" id = "Nome"/>
                            </div>
                            <div>
                            <label For="sala">sala: </label><br/>
                            <input type="text" id = "sala"/>
                            </div>
                        </section>
                        <section className="calendario-turmasadd">
                            <div>
                            <label For="Calendario">Calendário: </label><br/>
                            <input type="text" id = "Calendario"/>
                            </div>
                            <div>
                            <label For="Materia">Matéria: </label><br/>
                            <select name = "Materia" id = "Materia"></select>
                            </div>
                        </section>
                        <section className="professor-turmaadd">
                            <div>
                            <label For="Professor">Professor: </label><br/>
                            <select name = "Professor" id = "Professor"></select>
                            </div>
                        </section>
                        </form>
                        <input type="button" className="botao-turmasadd-salvar" value="Salvar" />
                </div>
            </section>
        </body>
        </>
    )
}