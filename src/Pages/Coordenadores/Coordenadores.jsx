import React, {useContext, useState, useEffect} from 'react'
import './Coordenadores.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import { Context } from "../../Context/Context";
import { Link } from 'react-router-dom';

export default function Coordenadores() {
    let {PegarCoordenadores} = useContext(Context)
    const [coordenador, setCoordenador] = useState([])
    
    useEffect(() => {
        PegarCoordenadores(setCoordenador)
    }, [])

    useEffect(() => {
        // console.log(aluno)
    }, [coordenador])
    return(
        <>
            <body>
                <section className="borda-coordenador">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div className="borda-geral-coordenadores">
                    <section className="botao-coordenador">
                    <div>
                            <Link to ="/coordenadoresedicao">
                            <button>+</button>
                            </Link>
                    </div>
                    </section>
                    <section className="topo-coordenador">
                        <div>
                            <h1>Coordenadores</h1>
                        </div>
                    </section>
                    <br/>
                    <section className="topo-tabela-coordenador">
                        <div>
                            <p>Nome</p>
                        </div>
                        <div>
                            <p>CPF</p>
                        </div>
                        <div>
                            <p>RG</p>
                        </div>
                        <div>
                            <p>Depart/<br/>Curso</p>
                        </div>
                    </section>
                    <section className="corpo-coordenador">
                    {coordenador.map( bloco => {
                    return (<div className="classCoordenador">
                        <div>
                        <div className="nome-coord">
                            <p>{bloco.name}</p>
                        </div>
                        </div>
                        <div>
                            <p>{bloco.cpf}</p>
                        </div>
                        <div>
                        <div className="bloco-rg">
                            <p>{bloco.RG}</p>
                        </div>
                        </div>
                        <div>
                            <p>GMA</p>
                        </div>
                </div>)
                    })
                }
                    </section>
                    </div>
                </section>
            </body>
        </>
    )
}