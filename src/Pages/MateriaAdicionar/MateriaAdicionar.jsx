import React from 'react'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'
import './MateriaAdicionar.css'

export default function MateriaAdicionar() {
    return(
        <>
        <body>
            <section className="borda-adicionar-materias">
                 <div>
                <BarraLateral/>
                </div>
                <div className="borda-geral-adicionar-materias">
                    <section className="adicionar-materias">
                        <div><h1>Adicionar matérias</h1></div>
                    </section>
                    <form action ="">
                        <section className="nome-materias-adicionar">
                            <div>
                            <label For="nome">Nome:</label><br/>
                            <input type="text" id = "nome"/>
                            </div>
                        </section>
                        <section className="form-materias-adicionar">
                        <div>                            
                            <label For="Area">Área do conhecimento:</label><br/>
                            <input type="text" id = "Area"/>
                        </div>
                        <div>
                            <label For="CHT">CHT:</label><br/>
                            <input type="number" id = "CHT"/>
                        </div>
                        </section>
                        <section className="pre-requisito">
                            <div>
                            <label For="pre">Adicionar pré-requisito</label><br/>
                            <select name="" id="pre">
                            <option value="AC">pre</option>
                            </select>
                            </div>
                            <div>
                            <input type="submit" value="Adicionar" className="botao-adicionar-materias" />
                            </div>
                        </section>
                        <section className="top-tabela-materias-adicionar">
                            <div>
                                <p>Pré-requisitos</p>
                            </div>
                        </section>
                        <section className="tabela-adicionar-materias">
                            <div>
                                <p>Calcula I</p>
                            </div>
                            <div>
                                <p>Matemática</p>
                            </div>
                            <div>
                            <input type="submit" value="Excluir" className="botao-excluir-materias" />
                            </div>
                        </section>
                    </form>

                </div>
                </section>
                    {/* função api */}
                {/* /* função api * */}
        </body>
        </>
    )
}