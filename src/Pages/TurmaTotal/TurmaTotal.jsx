import React from 'react'
import './TurmaTotal.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'

export default function TurmaTotal(){
    return (
        <>
        <section className="bordas-turma-total">
            <div>
                <BarraLateral/>
            </div>
            <div className="borda-geral-turma-total">
            <section className="botao-turmas-total">
                <div>
                <Link to ="/TurmaTotalAdd">
                    <button>+</button>
                </Link>
                </div>
            </section>
            <section className="topo-turmas-total">
                <div>
                <h1>Turmas</h1>
                </div>
            </section>
            <section className="periodo-letivo">
                <div>
                <p>Ano letivo: 2021.1</p>
                <p>Status: NOMESTATUS</p>
                </div>
            </section>
            <section className="Calculo">
                <div>
                    <p>Calculo I</p>
                </div>
            </section>
            <section className="topo-tabela-turmas">
                <div>
                    <p>Nome</p>
                </div>
                <div>
                    <p>Calendário</p>
                </div>
                <div>
                    <p>Professor</p>
                </div>
                <div>
                    <p>vagas</p>
                </div>
            </section>
            <section className="horario-turmas">
                <div>
                    <p>Turma A1</p>
                </div>
                <div>
                    <p>Seg e Qua, 14h às 16h</p>
                </div>
                <div>
                    <p>Paulo de Souza</p>
                </div>
                <div>
                    <p>80</p>
                </div>
            </section>
            <section className="horario-turmas">
                <div>
                    <p>Turma A1</p>
                </div>
                <div>
                    <p>Seg e Qua, 14h às 16h</p>
                </div>
                <div>
                    <p>Paulo de Souza</p>
                </div>
                <div>
                    <p>40</p>
                </div>
            </section>
            <section className="Calculo">
                <div>
                    <p>Calculo I</p>
                </div>
            </section>
            <section className="horario-turmas">
                <div>
                    <p>Turma A1</p>
                </div>
                <div>
                    <p>Seg e Qua, 14h às 16h</p>
                </div>
                <div>
                    <p>Paulo de Souza</p>
                </div>
                <div>
                    <p>80</p>
                </div>
            </section>
            <div>
                {/* FUNÇÃO API */}
            </div>
        </div>
        </section>
        </>
    )
}