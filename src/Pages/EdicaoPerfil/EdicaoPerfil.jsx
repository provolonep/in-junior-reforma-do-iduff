import React, {useContext, useState} from 'react'
import './EdicaoPerfil.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import { Context } from "../../Context/Context"

let user = JSON.parse(localStorage.getItem('userLog'))



export default function EdicaoPerfil(){
    let {EditarPerfil} = useContext(Context)
    let {EditarSenha} = useContext(Context)

    const [Nome, setNome] = useState('')
    const [Rua, setRua] = useState('')
    const [Numero, setNumero] = useState('')
    const [Bairro, setBairro] = useState('')
    const [Complemento, setComplemento] = useState('')
    const [Estado, setEstado] = useState('')
    const [Cep, setCep] = useState('')
    const [Telefone, setTelefone] = useState('')
    const [Celular, setCelular] = useState('')
    const [Senha1, setSenha1] = useState('')
    const [Senha2, setSenha2] = useState('')

    return(
        <>
        <body>
            <section className="borda-perfil">
                <div>
                <BarraLateral/>
                </div>
                <div className="borda-geral-perfil">
                <section className="topo-perfil">
                    <div>
                    <h1>Editar Perfil</h1>
                    </div>
                </section>
                <form action="" method = "PUT" onSubmit = {e => EditarPerfil(Nome, Rua, Numero, Bairro, Complemento, Estado, Cep, Telefone, Celular, user.id, e)}>
                <section className="sub-perfil">
                    <div>
                    <p>Editar os dados cadastrais da sua conta IDUFF</p>
                    </div>
                </section>
                <section className="nome-perfil">
                    <div>
                    <label for = "nome" >Nome Completo: </label><br/>
                    <input  type = 'text' id = "nome" className="input-perfil" onChange= {e => setNome(e.target.value)}/>
                    </div>
                </section>
                <section className="nacionalidade-perfil">
                    <div>
                    <p>Nacionalidade: {user.nationality}</p></div>
                    <div><p>Estado: {user.state}</p></div>
                    <div><p>RG: {user.RG}  Cpf:{user.cpf}</p></div>
                    <br/>
                    <div><p>Dados para contato:</p></div>
                </section>
                <section className="form-perfil">
                    <div className="div-perfil">
                    <label for = "rua" id ="label-rua">Rua: </label><br/>
                    <input type = 'text'  id = "rua" onChange= {e => setRua(e.target.value)}/>
                    </div>
                    <div className="div-perfil"><br/>
                    <label for = "N" >Nº: </label><br/>
                    <input type = "number" id = "N" onChange= {e => setNumero(e.target.value)}/>
                    </div>
                    <div className="div-perfil"><br/>
                    <label for  = "nome" >Bairro </label><br/>
                    <input type = "nome" id = "Bairro" onChange= {e => setBairro(e.target.value)}/>
                    </div>
                </section>
                <section className="form-perfil">
                    <div>
                    <label for  = "complemento" >Complemento: </label><br/>
                    <input type = "text" id = "complemento" onChange= {e => setComplemento(e.target.value)}/>
                    </div>
                    <div>
                    <label for  = "Estado" >Estado: </label><br/>
                    <input type = "text" id = "Estado" onChange= {e => setEstado(e.target.value)}/>
                    </div>
                    <div>
                    <label for  = "CEP" >CEP </label><br/>
                    <input type = "number" id = "CEP" onChange= {e => setCep(e.target.value)}/>
                    </div>
                </section>
                <section className="form-perfil">
                    <div>
                    <label for  = "telefone" >Telefone </label><br/>
                    <input type = "number" id = "telefone" onChange= {e => setTelefone(e.target.value)}/>
                    </div>
                    <div>
                    <label for  = "celular" >Celular </label><br/>
                    <input type ="number" id = "celular" onChange= {e => setCelular(e.target.value)}/>
                    </div>
                    </section>
                    </form>
                    <form action="" method = "PUT" onSubmit = {e => EditarSenha(Senha1, Senha2, user.id, e)}>
                    <section className="footer-form-perfil">
                        <div>
                        <label for  = "senha1" >Senha </label><br/>
                        <input type ="password" id = "senha1" onChange= {e => setSenha1(e.target.value)}/>
                        </div>
                        <div>
                        <label for  = "senha2" >Repita sua senha </label><br/>
                        <input type ="password" id = "senha2" onChange= {e => setSenha2(e.target.value)}/>
                        </div>
                        <div>
                        <input type="submit" value="Trocar" className="botao-edicao-senha"/>
                        </div>
                    </section>
                    <input type="submit" value="Salvar" className="botao-edicao"/>
                    </form>
            
            </div>
        </section>
        </body>
        </>
    )
}