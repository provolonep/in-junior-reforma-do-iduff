import React from 'react'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'
import './PeriodoLetivo.css'

export default function Periodo_Letivo() {
    let userLog = JSON.parse(localStorage.getItem('userLog'))
    return(<>
    <body>
        <section className="bordas-letivo">
            <div>
                <BarraLateral/>
            </div>
            <div className="borda-geral-periodo">
            <section className="topo-letivo">
                <h1>Periodo letivo</h1>
            </section>
            <section className="cabecalho-letivo">
                <div>
                    <p>Ano letivo atual: NOMEDOANOLETIVO<br/><br/>Staus: 2021.1</p>
                </div>
                 <div>
                {(userLog.role == 'studant') && <input type="submit" value="Planejar:2021.1" /> }
                </div>
            </section>
            <section className="materias-letivo">
                <div>
                <p>matérias oferecidas pelo departamento</p>
                </div>
                <div>
                <Link to ="/adicionar_materia"> 
                    <input type="submit" value="Adicionar Matéria" />
                </Link>
                </div>
            </section>
            <section className="corpo-letivo">
                <div>
                <p>Nome</p>
                </div>
                <div>
                <p>Área do conhecimento</p>
                </div>
                <div>
                <p>Total de vagas</p>
                </div>
            </section>
            <section className="materias-anoletivo">
                <div>
                    <p>Cálculo I</p>
                </div>
                <div>
                    <p>Matemática</p>
                </div>
                <div>
                    <p>120</p>
                </div>
                <div>
                    <input type="submit" className="botao-editar-periodo" value="Editar" />
                </div>
            </section>
            <div>
        </div>
        {/* função api */}
            </div>
        </section>

    </body>
    </>)
}