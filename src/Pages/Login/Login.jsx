import React, {useContext, useState} from 'react'
import './Login.css'
import {Link} from 'react-router-dom'
import { Context } from "../../Context/Context"
import axios from 'axios'

function Login() {
    let {handleLogin} = useContext(Context)
    const [Password, setPassword] = useState('')
    const [Cpf, setCpf] = useState('')

    return(
        <>
            <section id="pag-log">
                <div>
                    <h1 className="UFF">idUFF</h1>
                    <h2 className="sub-uff">Bem vindo ao Sistema<br />
                    Acadêmico da Graduação</h2>
                    <p className="texto">Lorem, ipsum dolor sit amet consectetur<br />
                    adipisicing elit. Impedit repellat harum tempore,
                    sed placeat nesciunt rem id voluptate. Nesciunt aut
                    eligendi ipsum porro velit est facere maxime temporibus
                        cum modi?</p>
                </div>
                <div className="moldura">
                <h1 className="Acesse">Acesse o seu idUFF</h1><br />
                <form action="" method ="POST" id="formulario" onSubmit={e => handleLogin(Cpf, Password, e)}>
                    <div class="form1">
                        <label for = "nome">Seu CPF (somente números)</label>
                        <input name="cfp" type="text" id="cpf" onChange={e => setCpf(e.target.value)}/>
                        <br />
                        <label for = "nome">Senha do seu idUFF</label>
                        <input name="id" type="password" id="senha" onChange={e => setPassword(e.target.value)}/>
                    </div>
                        <input type="submit" className="botao-login" value="Logar" />
                </form>
                </div>
                </section>
        </>
    )  
}

export default Login