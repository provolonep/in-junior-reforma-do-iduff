import React from 'react'
import './Materias.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'


function Materias() {
    return(
        <>
            <body>
                <section className="borda-materias">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div>
                    <div className="div-borda-geral">
                    <section className="topo-materias">
                    <div>
                        <h1>Adicionar matérias</h1>
                    </div>
                    </section>
                    <form action="" method="POST">
                    <section className="form-nome-materias">
                        <div>
                        <label for = "nome">Nome:</label><br/>
                        <input name="nome" type="text" id="nome" value=""/>
                        </div>
                    </section>
                    <section className="form-materias">
                        <div>
                        <label for = "area">Area de Conhecimento:</label><br/>
                        <input name="nome" type="text" id="area" value=""/> 
                        </div>
                        <div>
                        <label for = "cht">CHT:</label><br/>
                        <input name="" type="number" id="nome" value=""/>
                        </div>
                    </section>
                    <section className="form-adicionar">
                        <div>
                        <label for = "area">Adicionar pré-requisito:</label>
                        <select name="select"></select>
                        </div>
                        <div>
                        <input type="button" className="botao-adicionar-materias" value="Adicionar" />
                        </div>
                    </section>
                    </form>
                    <section className="requisitos-materias">
                        <div>
                        <p>Pré-requisitos</p>
                        </div>
                    </section>
                    <section className="tabela-materias">
                    <div>
                        <p>Calculo I</p>
                    </div>
                    <div>
                        <p>Matemática</p>
                    </div>
                    <div>
                        <input type="button" className="botao-materias-excluir" value="Excluir" />
                    </div>
                    </section>
                    <input type="button" className="botao-materias" value="Salvar" />
                    </div>
                    </div>
                </section>
            </body>
        </>
    )
}

export default Materias