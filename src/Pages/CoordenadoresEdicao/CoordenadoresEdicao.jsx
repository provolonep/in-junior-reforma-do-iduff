import React, {useContext, useState} from 'react'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'
import './CoordenadoresEdicao.css'
import { Context } from "../../Context/Context"
import { AdicionarCoodernador } from '../../Services/api'

export default function CoordenadoresEdicao(){
    let {AdicionarCoordenador} = useContext(Context)

    const [Rg, setRg] = useState('')
    const [Date, setDate] = useState('')
    const [Cpf, setCpf] = useState('')
    const [Email, setEmail] = useState('')
    const [Nome, setNome] = useState('')
    const [Nacionalidade, setNacionalidade] = useState('')
    const [Papel, setPapel] = useState()
    const [Estado, setEstado] = useState('')
    return(
     <>
        <section className="borda-coord-edicao">
            <div>
                <BarraLateral/>
            </div>
            <div className="borda-geral-coordenadores-edicao">
            <section className="topo-coord-edicao">
                <div>
                <h1>Coordenadores</h1>
                </div>
            </section>
         <form >
        <section className="select-coord">
            <p>Tipo</p>
        <select name="Estado" id="Estado" onChange = {e => setPapel(e.target.value)}>
                 <option value="3">Departamento</option>
                 <option value="2">Curso</option>
        </select>
        </section>
        <section className="form-coordenador-edicao">
            <div>
            <label for = "nome1" >Nome Completo: </label><br/>
            <input name="nome" type = 'text' id = "nome1"onChange = {e => setNome(e.target.value)}/>
            </div>
        </section>
        <section className="form-coordenador1-edicao">
            <div>
            <label for = "RG" >RG: </label><br/>
            <input name="RG" type = 'text' id = "RG"onChange = {e => setRg(e.target.value)}/>
            </div>
            <div>
            <label for = "Estado" >Estado: </label><br/>
             <select name="Estado" id="Estado"onChange = {e => setEstado(e.target.value)}>
                 <option value="AC">Acre</option>
                 <option value="AL">Alagoas</option>
                 <option value="AP">Amapá</option>
                 <option value="AM">Amazonas</option>
                 <option value="BA">Bahia</option>
                 <option value="CE">Ceará</option>
                 <option value="DF">Distrito Federal</option>
                 <option value="ES">Espírito Santo</option>
                 <option value="GO">Goiás</option>
                 <option value="MA">Maranhão</option>
                 <option value="MT">Mato Grosso</option>
                 <option value="MS">Mato Grosso do Sul</option>
                 <option value="MG">Minas Gerais</option>
                 <option value="PA">Pará</option>
                 <option value="PB">Paraíba</option>
                 <option value="PR">Paraná</option>
                 <option value="PE">Pernambuco</option>
                 <option value="PI">Piauí</option>
                 <option value="RJ">Rio de Janeiro</option>
                 <option value="RN">Rio Grande do Norte</option>
                 <option value="RS">Rio Grande do Sul</option>
                 <option value="RO">Rondônia</option>
                 <option value="RR">Roraima</option>
                 <option value="SC">Santa Catarina</option>
                 <option value="SP">São Paulo</option>
                 <option value="SE">Sergipe</option>
                 <option value="TO">Tocantins</option>
             </select>
            </div>
        </section>
        <section className="form-coordenador1-edicao">
            <div>
            <label for = "Nascimento" >Nascimento: </label><br/>
            <input name="Nascimento" type = "date" id = "Nascimento" onChange = {e => setDate(e.target.value)}/>
            </div>
            <div>
            <label for = "Matricula" >Matrícula: </label><br/>
            <input name="Matricula" type = 'text' id = "Matricula"/>
            </div>
        </section>
        <section className="form-coordenador1-edicao">
            <div>
            <label for = "email" >Email: </label><br/>
            <input name="email" type = 'email' id = "email" onChange = {e => setEmail(e.target.value)}/>
            </div>
            <div>
            <label for = "cpf" >CPF: </label><br/>
            <input name="cpf" type = 'text' id = "cpf" onChange = {e => setCpf(e.target.value)}/>
            </div>
        </section>
            <div><p>**Será atribuido o CPF como senha temporária**</p></div>
             <button type="submit" className="botao-coord-salvar" onClick = {e => AdicionarCoordenador(Rg, Date, Email, Cpf, Nome, Papel, Estado, e)}>Enviar</button>
         </form>
            </div>
        </section>
         
     </>
     )
 }