import React from 'react'
import './Departamento.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'

function Departamento() {
    return(
        <>
            <body>
                <section className="divdepart">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div className="borda-geral-departamento">
                    <section className="topo-departamento">
                        <div>
                        <h1>Departamento</h1>
                        </div>
                    </section>
                    <section className="sub-departamento">
                    <div>
                        <h3>Edite as informações do departamento</h3>
                    </div>
                    </section>
                    <form action="" method ="POST" id="form2">
                    <section className="form-departamento">
                    <div>
                        <label for = "nome">Nome do Departamento:</label><br/>
                        <input name="nome" type="text" id="nome" value=""/>
                    </div>
                    <br />
                    <div>
                        <label for = "nome">Área de conhecimento:</label><br/>
                        <input name="texto" type="text" id="nome" value=""/>
                    </div>
                    <br />
                    <div>
                        <label for = "nome">Campus/Sede:</label><br/>
                        <input name="nome" type="text" id="nome" value=""/>
                    </div>
                    <br />
                    </section>
                    <section className="form-footer">
                    <div>
                        <label for = "nome">Dados para contato Telefone:</label><br/>
                        <input name="nome" type="number" id="nome" value=""/>
                    </div>
                    <div>
                        <label for = "nome">Coordenador:</label><br/>
                        <select name="select"></select>
                    </div>
                    <div>
                        <label for = "nome">Email:</label><br/>
                        <input name="nome" type="email" id="nome" value=""/>
                    </div>
                    </section>
                    <section className="footer-departamento">
                    <div>
                    <Link to='/periodoletivo'>
                            <h3>Visualizar Ano Letivo</h3>
                        </Link>
                    </div>
                    </section>
                    <button className="botao-departamento">Salvar</button>
                    </form>
                    </div>
                </section>
            </body>
        </>
    )
}

export default Departamento