import React, {useContext, useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import { Context } from "../../Context/Context";
import './Professores.css'

export default function ProfessoresLista() {

    let {PegarProfessores} = useContext(Context)
    const [professor, setProfessor] = useState([])
    
    useEffect(() => {
        PegarProfessores(setProfessor)
    }, [])

    useEffect(() => {
    }, [professor])

    return(
        <>
            <body>
                <section className="borda-professor">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div className="borda-geral-professores">
                    <section className="botao-professor">
                    <div>
                        <Link to = "/professores">
                            <input type="submit" value="+" />
                        </Link>
                    </div>
                    </section>
                    <section className="topo-professor">
                        <div>
                            <h1>Professores</h1>
                        </div>
                    </section>
                    <br/>
                    <section className="topo-tabela-professor">
                        <div>
                            <p>Nome</p>
                        </div>
                        <div>
                            <p>CPF</p>
                        </div>
                        <div>
                            <p>RG</p>
                        </div>
                        <div>
                            <p>Turmas</p>
                        </div>
                    </section>
                    <section className="corpo-professor">
                    {professor.map( bloco => {
                    return (<div className="classprofessor">
                        <div>
                            <p>{bloco.name}</p>
                        </div>
                        <div>
                            <p>{bloco.cpf}</p>
                        </div>
                        <div>
                            <p>{bloco.RG}</p>
                        </div>
                        <div>
                            <p>6</p>
                        </div>
                        <div>
                            <p>Turma1</p>
                        </div>
                </div>)
            
        })
    } 
                    </section>
                    </div>
                </section>
                <div>  {/* Função que vai pegar os cards da API e retornar */}
                </div>  
            </body>
        </>
    )
}