import React from 'react'
import './InscricaoMaterias.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'

export default function alunos() {
    return(
        <>
            <body>
                <section className="borda-inscricao">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div className="borda-geral-materias">
                    <section className="topo-inscricao">
                        <div>
                            <h1>Inscrição em matérias</h1>
                        </div>
                    </section>
                    <br/><br/>
                    <section className="topo-tabela-inscricao">
                        <div>
                            <p>Nome</p>
                        </div>
                        <div>
                            <p>Vagas</p>
                        </div>
                        <div>
                            <p>Período</p>
                        </div>
                        <div>
                            <p>CHT</p>
                        </div>
                    </section>
                    <br/>
                    <section className="corpo-inscricao">
                        <div>
                            <p>
                                Cálculo I
                            </p>
                        </div>
                        <div>
                            <p>
                                13/45
                            </p>
                        </div>
                        <div>
                            <p>
                                1º período
                            </p>
                        </div>
                        <div>
                            <p>
                                60
                            </p>
                        </div>
                        <div>
                            <button className="botao-inscrever">Inscrever</button>
                        </div>
                    </section>
                    </div>
                </section>
                <div>  {/* Função que vai pegar os cards da API e retornar */}
                </div>  
            </body>
        </>
    )
}
