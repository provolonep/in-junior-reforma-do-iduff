import React, {useContext, useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import { Context } from "../../Context/Context";
import './Curriculos.css'

export default function Curriculos(){
    let {PegarCursos} = useContext(Context)
    let {PegarMaterias} = useContext(Context)
    const [curso, setCurso] = useState([])
    const [materia, setMateria] = useState([])
    const [materiaid, setMateriaid] = useState([])
    
    useEffect(() => {
        PegarCursos(setCurso)
    }, [])
    
    useEffect(() => {
        PegarMaterias(setMateria)
    }, [])
    

    useEffect(() => {
        console.log(curso)
    }, [curso])
    useEffect(() => {
    }, [materia])
    return(
        <>
            <body>
                <section className="borda-curriculo">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div className="borda-geral-curriculos">
                        <section className="sobre-curriculo">
                        <div>
                            <h1>Visualizar currículos</h1>
                        </div>
                        </section>
                        <section className="corpo-curriculo">
                            <div>
                                <p>Visualize aqui o currículo de qualquer um dos cursos da uff</p><br/>
                                <label for = "curso" >Curso:</label>
                                <select name="Ecurso" id="Curso">
                                {/* Os cursos de exmeplo estão duplicados mesmo, se voce ver no console log, cada um tem seu próprio ID */}
                                {curso.map( bloco => {
                                    return(<option value="TO">{bloco.name}</option>)
            
        })
    }   
                                </select>
                            </div>
                        </section>
                        <section className="titulo-curriculo">
                            <div>
                                <p>Nome</p>
                            </div>
                            <div>
                                <p>Período</p>
                            </div>
                            <div>
                                <p>CHT</p>
                            </div>
                        </section>
                        <section className="tabela-curriculo">
                        {/* {materia.map( bloco => {
                                    return(console.log(bloco)
                        )
                            })
                        }    */}
                                    </section>
                        </div>
                </section>
                </body>
                </>
    )
}