import React from 'react'
import './Historico.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'

export default function alunos() {
    return(
        <>
            <body>
                <section className="borda-historico">
                    <div>
                        <BarraLateral/>
                    </div>
                    <div className="borda-geral-historico">
                    <section className="topo-historico">
                        <div>
                            <h1>Histórico</h1>
                        </div>
                    </section>
                    <br/>
                    <section className="topo-tabela-historico">
                        <div>
                            <p>Nome</p>
                        </div>
                        <div>
                            <p>Status</p>
                        </div>
                        <div>
                            <p>Nota</p>
                        </div>
                    </section>
                    <section className="corpo-historico">
                        <div>
                            <p>
                                Cáculo I
                            </p>
                        </div>
                        <div>
                            <p>
                                Aprovado
                            </p>
                        </div>
                        <div>
                            <p>
                                9.0
                            </p>
                        </div>
                    </section>
                    </div>
                </section>
                <div>  {/* Função que vai pegar os cards da API e retornar */}
                </div>  
            </body>
        </>
    )
}
