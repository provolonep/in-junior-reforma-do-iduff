import React from 'react'
import './Turmas.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'

export default function Turmas() {
    let userLog = JSON.parse(localStorage.getItem('userLog'))
    
    return(
        <>
        <body>
            <section className="borda-turmas">
                <div>
                    <BarraLateral/>
                </div>
                <div className="borda-geral-turmas">
                <section className="topo-turmas">
                    <div>
                        <h1>Turmas</h1>
                    </div>
                </section>
                <section className="cargo">
                    <div>
                    <p>CARGO: NOME</p>
                    </div>
                </section>
                <section className="topo-tabela">
                <div>
                    <p>Nome</p>
                </div>
                <div>
                    <p>Código</p>
                </div>
                <div>
                    <p>Calendário</p>
                </div>
                <div>
                    <p>Sala</p>
                </div>
                </section>
                <section className="tabela-turmas">
                    <div>
                        Cáculo I
                    </div>
                    <div>
                        A1
                    </div>
                    <div>
                        Seg e Qua, 14h as 16h
                    </div>
                    <div>
                        401
                    </div>
                    <div>
                    {(userLog.role === 'student') && <><Link to ="/TurmaTotal"><input type="submit" className="botao-gerenciar-turmas" value="Gerenciar"></input></Link></>}
                    </div>
                </section>
                <section className="tabela-turmas">
                    <div>
                        Cáculo I
                    </div>
                    <div>
                        A1
                    </div>
                    <div>
                        Seg e Qua, 14h as 16h
                    </div>
                    <div>
                        401
                    </div>
                    <div>
                    {(userLog.role === 'student') && <><input type="submit" className="botao-gerenciar-turmas" value="Gerenciar"></input></>}
                    </div>
                </section>
                <div>
                    {/* Função que vai pegar os cards da API e retornar */}
                </div>
                </div>
            </section>    
            
        </body>
        </>
    )
}
