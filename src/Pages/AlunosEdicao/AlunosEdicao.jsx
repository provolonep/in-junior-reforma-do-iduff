import React, {useContext, useState} from 'react'
import './AlunosEdicao.css'
import { Link } from 'react-router-dom';
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import { Context } from "../../Context/Context"

let userLog = JSON.parse(localStorage.getItem('userLog'))

function Edicao(){
    let {AdicionarAluno} = useContext(Context)

    const [Nome, setNome] = useState('')
    const [Rg, setRg] = useState('')
    const [Estado, setEstado] = useState('')
    const [Date, setDate] = useState('')
    const [Matricula, setMatricula] = useState('')
    const [Email, setEmail] = useState('')
    const [Cpf, setCpf] = useState('')

    return(
     <>
        <section className="borda-alunos-edicao">
            <div>
                <BarraLateral/>
            </div>
            <div className="borda-geral-alunos-edicao">
            <section className="botao-edicao-alunos">
            <div>
            <Link to="/alunos">
                    <button>X</button>
                </Link>
            </div>
            </section>
            <section className="topo-alunos-edicao">
                <div>
                <h1>Alunos</h1>
                </div>
            </section>
        <section className="curso-alunos">
            <div>
                <p>Curso:</p>
            </div>
        </section>
        <section className="form-alunos-edicao">
            <div>
            <label for = "nome1" >Nome: </label><br/>
            <input name="nome" type = 'text' id = "nome1" onChange= {e => setNome(e.target.value)}/>
            </div>
        </section>
        <section className="form-alunos1-edicao">
            <div>
            <label for = "RG" >RG: </label><br/>
            <input name="RG" type = 'text' id = "RG"onChange = {e => setRg(e.target.value)}/>
            </div>
            <div>
            <label for = "Estado" >Estado: </label><br/>
             <select name="Estado" id="Estado" onChange = {e => setEstado(e.target.value)}>
                 <option value="AC">Acre</option>
                 <option value="AL">Alagoas</option>
                 <option value="AP">Amapá</option>
                 <option value="AM">Amazonas</option>
                 <option value="BA">Bahia</option>
                 <option value="CE">Ceará</option>
                 <option value="DF">Distrito Federal</option>
                 <option value="ES">Espírito Santo</option>
                 <option value="GO">Goiás</option>
                 <option value="MA">Maranhão</option>
                 <option value="MT">Mato Grosso</option>
                 <option value="MS">Mato Grosso do Sul</option>
                 <option value="MG">Minas Gerais</option>
                 <option value="PA">Pará</option>
                 <option value="PB">Paraíba</option>
                 <option value="PR">Paraná</option>
                 <option value="PE">Pernambuco</option>
                 <option value="PI">Piauí</option>
                 <option value="RJ">Rio de Janeiro</option>
                 <option value="RN">Rio Grande do Norte</option>
                 <option value="RS">Rio Grande do Sul</option>
                 <option value="RO">Rondônia</option>
                 <option value="RR">Roraima</option>
                 <option value="SC">Santa Catarina</option>
                 <option value="SP">São Paulo</option>
                 <option value="SE">Sergipe</option>
                 <option value="TO">Tocantins</option>
             </select>
            </div>
        </section>
        <section className="form-alunos1-edicao">
            <div>
            <label for = "Nascimento" >Nascimento: </label><br/>
            <input name="Nascimento" type = "date" id = "Nascimento" onChange = {e => setDate(e.target.value)}/>
            </div>
            <div>
            <label for = "Matricula" >Matrícula: </label><br/>
            <input name="Matricula" type = 'text' id = "Matricula"onChange = {e => setMatricula(e.target.value)}/>
            </div>
        </section>
        <section className="form-alunos1-edicao">
            <div>
            <label for = "email" >Email: </label><br/>
            <input name="email" type = 'email' id = "email" onChange = {e => setEmail(e.target.value)}/>
            </div>
            <div>
            <label for = "cpf" >CPF: </label><br/>
            <input name="cpf" type = 'text' id = "cpf" onChange = {e => setCpf(e.target.value)}/>
            </div>
        </section>
        <div><p>**Será atribuido o CPF como senha temporária**</p></div>
             <button className="botao-alunos-salvar" onClick = {e => AdicionarAluno(Nome, Rg, Estado,Email, Date, Matricula, Cpf, e)}>Salvar</button>
            </div>
        </section>
         
     </>
     )
 }
 export default Edicao
