import React from 'react'
import './TurmasGerencia.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'

export default function TurmasGerencia() {
    return(
        <>
        <body>
            <section className="gerencia">
                <div>
                    <BarraLateral/>
                </div>
                <div className="borda-gerencia-total">
                <section className="nometurmas">
                    <div>
                    <h1>Turmas</h1>
                    </div>
                </section>
                <section className="nomemateria">
                    <div>
                    <h2>NOMEMATERIA</h2>
                    </div>
                </section>
                <section className="codigo">
                    <div>
                    <h2>Turma NOMECODIGO</h2>
                    </div>
                </section>
                <section className="professorcalendario">
                    <div>
                    <p>Professor:NOMEPROFESSOR</p>
                    <p>Calendário: CALENDARIO</p>
                    </div>
                </section>
                <section className="lancamento">
                    <div>
                    <h3>Lançamento de notas</h3>
                    </div>
                </section>
                <br/>
                <section className="notas">
                    <div>
                        <p>
                            José dos Santos
                        </p>
                    </div>
                    <div>
                        <textarea name="" id="" cols="15" rows="3" placeholder="Nota 1">
                        </textarea>
                    </div>
                    <div>
                        <textarea name="" id="" cols="15" rows="3" placeholder="Nota 2">
                        </textarea>
                    </div>
                    <div>
                        <button className="botao-enviar-gereciar">Enviar</button>
                    </div>
                </section>
                <div>
                    {/* Função que vai pegar os cards da API e retornar */}
                </div>
                </div>
            </section>   
        </body>
        </>
    )
}
