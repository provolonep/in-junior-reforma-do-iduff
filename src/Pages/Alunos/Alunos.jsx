import React, {useContext, useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import { Context } from "../../Context/Context";
import './Alunos.css'



export default function Alunos() {
    let {PegarAlunos} = useContext(Context)
    const [aluno, setAluno] = useState([])
    
    useEffect(() => {
        PegarAlunos(setAluno)
    }, [])

    useEffect(() => {
        // console.log(aluno)
    }, [aluno])
    return(
        <>
        <section className="borda-alunos">
        <BarraLateral/>
        <div className="borda-geral-alunos">
        <section className="botao-alunos">
            <Link to ="/alunosedicao">
            <button>+</button>
            </Link>
            </section>
            <body>
            <section className="sobre">
                <h1>Alunos</h1>
            </section>
            <section className="topo-alunos">
            <div>
                <p>Nome</p>
            </div>
            <div>
                <p>CPF</p>
            </div>
            <div>
                <p>RG</p>
            </div>
            <div>
                <p>Período</p>
            </div>
            </section>
                <section className="corpo-alunos">
                    {aluno.map( bloco => {
                    return (<div className="classAlunos">
                        <div>
                            <p>{bloco.name}</p>
                        </div>
                        <div>
                            <p>{bloco.cpf}</p>
                        </div>
                        <div>
                            <p>{bloco.RG}</p>
                        </div>
                        <div>
                            <p>Período</p>
                        </div>
                </div>)
            
        })
    }                
                </section>
            </body>
        </div>
        </section>
        </>
    )
}