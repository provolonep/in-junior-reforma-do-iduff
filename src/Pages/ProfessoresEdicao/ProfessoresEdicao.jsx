import React from 'react'
import './ProfessoresEdicao.css'
import BarraLateral from '../../comoon/BarraLateral/BarraLateral'
import {Link} from 'react-router-dom'

export default function Professores() {
    return(
        <>
             <section className="borda-professoresedit-edicao">
            <div>
                <BarraLateral/>
            </div>
            <div className="borda-geral-professor-edicao">
            <section className="botao-professoresedit-alunos">
            <div>
                <Link to = "/professoreslista">
                    <button>x</button>
                </Link>
            </div>
            </section>
            <section className="topo-professoresedit-edicao">
                <div>
                <h1>Professores</h1>
                </div>
            </section>
        <section className="curso-professoresedit">
            <div>
                <p>Departamento:</p>
            </div>
        </section>
         <form action="">
        <section className="form-professoresedit-edicao">
            <div>
            <label for = "nome1" >Nome: </label><br/>
            <input name="nome" type = 'text' id = "nome1"/>
            </div>
        </section>
        <section className="form-professoresedit-edicao">
            <div>
            <label for = "RG" >RG: </label><br/>
            <input name="RG" type = 'text' id = "RG"/>
            </div>
            <div>
            <label for = "Estado" >Estado: </label><br/>
             <select name="Estado" id="Estado">
                 <option value="AC">Acre</option>
                 <option value="AL">Alagoas</option>
                 <option value="AP">Amapá</option>
                 <option value="AM">Amazonas</option>
                 <option value="BA">Bahia</option>
                 <option value="CE">Ceará</option>
                 <option value="DF">Distrito Federal</option>
                 <option value="ES">Espírito Santo</option>
                 <option value="GO">Goiás</option>
                 <option value="MA">Maranhão</option>
                 <option value="MT">Mato Grosso</option>
                 <option value="MS">Mato Grosso do Sul</option>
                 <option value="MG">Minas Gerais</option>
                 <option value="PA">Pará</option>
                 <option value="PB">Paraíba</option>
                 <option value="PR">Paraná</option>
                 <option value="PE">Pernambuco</option>
                 <option value="PI">Piauí</option>
                 <option value="RJ">Rio de Janeiro</option>
                 <option value="RN">Rio Grande do Norte</option>
                 <option value="RS">Rio Grande do Sul</option>
                 <option value="RO">Rondônia</option>
                 <option value="RR">Roraima</option>
                 <option value="SC">Santa Catarina</option>
                 <option value="SP">São Paulo</option>
                 <option value="SE">Sergipe</option>
                 <option value="TO">Tocantins</option>
             </select>
            </div>
        </section>
        <section className="form-professoresedit-edicao">
            <div>
            <label for = "Nascimento" >Nascimento: </label><br/>
            <input name="Nascimento" type = "date" id = "Nascimento"/>
            </div>
            <div>
            <label for = "Matricula" >Matrícula: </label><br/>
            <input name="Matricula" type = 'text' id = "Matricula"/>
            </div>
        </section>
        <section className="form-professoresedit-edicao">
            <div>
            <label for = "Senha" >Senha: </label><br/>
            <input name="Senha" type = 'password' id = "Senha"/>
            </div>
            <div>
            <label for = "cpf" >CPF: </label><br/>
            <input name="cpf" type = 'number' id = "cpf"/>
            </div>
        </section>
        <section className="select-materia">
            <div>
                <p>Adicionar matéria para o professor</p><br/>
            </div>
            <div>
                <select name="materia" id="materia-prof"></select>
            </div>
            <div>
            <input type="button" className="botao-professoresedit-adicionar" value="Adicionar" />
            </div>
        </section>
         </form>
         <section className="form2-professores">
             <div>
                 <p>
                     Cálculo I
                 </p>
             </div>
             <div>
                 <p>Matemática</p>
             </div>
             <div>
             <input type="button" className="botao-professores-excluir" value="Excluir" />
             </div>
         </section>
         <input type="button" className="botao-professoresedit-salvar" value="Salvar" />
            </div>
        </section>
         
     </>
     )
 }
