import React, { useContext } from 'react'
import { Link } from "react-router-dom";
import { Context } from '../../Context/Context'
import './BarraLateral.css'

function BarraLateral() {
    let {handleLogout} = useContext(Context)
    let userLog = JSON.parse(localStorage.getItem('userLog'))
    return(
    <>
    <section className="topo-barra">
    <div class="tag-tp">
        <p className="tag-p">{userLog.name}</p>
        <p className="tag-p">{userLog.role}</p>
        <p className="tag-p">{userLog.registration}</p>
    </div>
        <Link to="/perfil">
        <input type="submit" className="botao-editar" value="Editar" />
        </Link>
    <nav className="navegacao-menu">
    <ul>
            {(userLog.role === 'coursecoordinator') &&
            <><li><Link to ="/alunos">Alunos</Link></li><br/></>}
            
            {(userLog.role === 'student') &&
            <><Link to="/historico"><li>Histórico</li></Link><br/></>}
            
            {(userLog.role === 'departmentcoordinator'|| userLog.role === 'principal' ) &&
            <><Link to ="/departamento"><li>Departamento</li></Link><br/></>}
            
            {(userLog.role === 'principal') &&
            <><Link to ="curso"><li>Cursos</li></Link><br/></>}
            
            {(userLog.role === 'principal' ) &&
            <><Link to = "coordenadores"><li>coordernadores </li></Link><br/></>}
            
            {(userLog.role === 'coursecoordinator') &&
            <><Link to = "curso"><li>Curso</li></Link><br/></>}
            
            
            <Link to ="/curriculos"><li>Currículos</li></Link><br/>
            
            {(userLog.role === 'departmentcoordinator')&&
            <><Link to='/periodoletivo'><li>2020.2</li></Link><br/></>}
            
            {(userLog.role === 'departmentcoordinator')&&
            <><Link to = "/professoreslista"><li>Professores</li></Link><br/></>}

            {(userLog.role === 'student' || userLog.role === 'teacher' ) 
            &&<><Link to ="/turmas"><li>Turmas</li></Link><br/></>}

            {(userLog.role === 'departmentcoordinator')&&
            <><Link to ="/turmatotal"><li>Turmas</li></Link><br/></>}

            {(userLog.role === 'student') && 
            <><Link to = "/inscricao"><li>Inscrição online</li></Link><br/></>}

            <li onClick = {e => handleLogout()}> Sair </li>
        </ul>
    </nav>
    </section>
</>)
} 

export default BarraLateral