import React, {createContext, useEffect, useState} from 'react'
import api from '../Services/api'
import history from '../history'
import axios, { credentials } from 'axios'

const Context = createContext()

function Provider({ children }) {
    const [authenticated, setAuthenticated] = useState(false)
    const [loading, setLoading] = useState(true)
    
    useEffect(() => {
        const token = localStorage.getItem('token')
        const userLog = localStorage.getItem('userLog')


        if (token) {
            api.defaults.headers.Authorization = `bearer ${JSON.parse(token)}`;
            setAuthenticated(true)
        }
        setLoading(false)
        
        if (userLog) {
            api.defaults.headers.Authorization = JSON.parse(userLog);
            
        }

    }, [])

    function handleLogin(Cpf,Password, e){
        e.preventDefault()
        
        const userLog = localStorage.getItem('userLog')

        let body = {"user":{
            "cpf": Cpf,
            "password": Password
        }}
        api.post('/login', body)
        .then(response => { 
            let token = response.data.token
            let user = response.data.user
            localStorage.setItem('token', JSON.stringify(token))
            localStorage.setItem('userLog', JSON.stringify(user));
            api.defaults.headers.Authorization = `bearer ${token}`
            setAuthenticated(true)
            window.location.reload(false); //solução temporária pois o push history nao estava funcionando como devia
        })
        .catch(error => {console.log(error)})
        history.push('/curriculos')
        
    }
    function handleLogout() {
        setAuthenticated(false)
        localStorage.removeItem('token');
        localStorage.removeItem('userLog')
        api.defaults.headers.Authorization = undefined;
        history.push('/Login')
    }
    function AdicionarCoordenador(Rg, Date, email, Cpf, Nome, Papel, Estado, e) {
        e.preventDefault()
        let token = JSON.parse(localStorage.getItem('token'))
        const body = {"user": { 
            "RG": Rg ,
            "birthdate": Date ,
            "email": email,
            "cpf": Cpf ,
            "name": Nome,
            "role": parseInt(Papel),
            "state": Estado
           }}
           api.post('/users', body, {headers: {'Authorization':`Bearer ${token}`}} )
            .then(response => console.log(response)
            ).catch(error => console.log(error))
      
        }
        function AdicionarAluno(Nome, Rg, Estado,Email, Date, Matricula, Cpf, e) {
            e.preventDefault()
            let token = JSON.parse(localStorage.getItem('token'))
            const body = {user: { 
                "name": Nome,
                "RG": Rg ,
                "email": Email,
                "state": Estado,
                "birthdate": Date,
                "registraion": Matricula,
                "cpf": Cpf ,
                "role": 0,
               }}
               api.post('/users', body, {headers: {'Authorization':`Bearer ${token}`}} )
                .then(response => console.log(response)
                ).catch(error => console.log(error))
          
            }

    async function PegarAlunos(setEstado) {
        let token = JSON.parse(localStorage.getItem('token'))
        await api.get('/users', {headers: {'Authorization':`Bearer ${token}`}})

            .then(resposta =>{
                let alunosList = []
                Object.keys(resposta.data).forEach( key => {
                    if (resposta.data[key].role === 'student')
                        alunosList.push(resposta.data[key])
                })
                setEstado(alunosList);
            }).catch(error =>{
                console.log(error);
            })
    }
    async function PegarProfessores(setEstado) {
        let token = JSON.parse(localStorage.getItem('token'))
        await api.get('/users', {headers: {'Authorization':`Bearer ${token}`}})

            .then(resposta =>{
                let professoresList = []
                Object.keys(resposta.data).forEach( key => {
                    if (resposta.data[key].role === 'teacher')
                        professoresList.push(resposta.data[key])
                })
                setEstado(professoresList);
            }).catch(error =>{
                console.log(error);
            })
    }
    async function PegarCoordenadores(setEstado) {
        let token = JSON.parse(localStorage.getItem('token'))
        await api.get('/users', {headers: {'Authorization':`Bearer ${token}`}})

            .then(resposta =>{
                let coordenadoresList = []
                Object.keys(resposta.data).forEach( key => {
                    if (resposta.data[key].role === 'departmentcoordinator' || resposta.data[key].role === 'coursecoordinator')
                    coordenadoresList.push(resposta.data[key])
                })
                setEstado(coordenadoresList);
            }).catch(error =>{
                console.log(error);
            })
    }
    async function PegarCursos(setEstado) {
        let token = JSON.parse(localStorage.getItem('token'))
        await api.get('/courses', {headers: {'Authorization':`Bearer ${token}`}})

            .then(resposta =>{
                let cursosList = []
                Object.keys(resposta.data).forEach( key => {
                    cursosList.push(resposta.data[key])
                })
                setEstado(cursosList);
            }).catch(error =>{
                console.log(error);
            })
        }
        async function PegarMaterias(setEstado) {
        let token = JSON.parse(localStorage.getItem('token'))
        await api.get('/subjects', {headers: {'Authorization':`Bearer ${token}`}})

        .then(resposta =>{
            let materiasList = []
            Object.keys(resposta.data).forEach( key => {
                // if (resposta.data[key].role == `${materia}`)
                materiasList.push(resposta.data[key])
            })
            setEstado(materiasList);
        }).catch(error =>{
            console.log(error);
        })
    }
    async function EditarPerfil(Nome, Rua, Numero, Bairro, Complemento, Estado, Cep, Telefone, Celular, id, e){
        e.preventDefault()
            let token = JSON.parse(localStorage.getItem('token'))
            const body = {user: { 
                "name": Nome,
                "street": Rua,
                "number": Numero,
                "district": Bairro,
                "complement": Complemento,
                "state": Estado,
                "cep": Cep,
                "telephone": Telefone,
                "cellphone": Celular,

               }}
                await api.put('/users/'+id, body, {headers: {'Authorization':`Bearer ${token}`}} )
                    .then(response => console.log(response)
                    ).catch(error => console.log(error))
          
            }

    async function EditarSenha(Senha1, Senha2, id, e){
        e.preventDefault()
            let token = JSON.parse(localStorage.getItem('token'))
            const body = {user: { 
                "password": Senha1,
                "password_confirmation": Senha2,

                }}
                if (Senha1 === Senha2){
                    await api.put('/users/'+id, body, {headers: {'Authorization':`Bearer ${token}`}} )
                        .then(response => console.log(response)
                        ).catch(error => console.log(error))
                        handleLogout()
                }
                else
                    alert("Senhas não são igauis")
            
            }
    
    if (loading) {
        return (<h1>laoding</h1>)
    }

    return(
    <Context.Provider value ={{ loading, authenticated, handleLogin, handleLogout,AdicionarCoordenador, AdicionarAluno, PegarAlunos ,PegarProfessores, PegarCoordenadores, PegarCursos, PegarMaterias ,EditarPerfil, EditarSenha }}>
        { children }
    </Context.Provider>
)}


export {Provider, Context}