import React, { createContext, useContext, useState } from 'react'
import './App.css';
import Routes from './routes';
import { Provider } from './Context/Context'
import Login from './Pages/Login/Login'

function App() {
  // const [token, setToken] = useState();

  // if(!token) {
  //   return <Login setToken={setToken} />
  // }
  return (
    <div className="App">
        <Provider>
          <Routes/>
        </Provider>
    </div>
  );
}

export default App;
