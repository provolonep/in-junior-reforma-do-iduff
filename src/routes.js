import React, { useContext } from 'react'

import { Router } from 'react-router'
import { Switch, Route, Redirect} from 'react-router-dom'
import Login from './Pages/Login/Login'
import Curriculos from './Pages/Curriculos/Curriculos'
import EdicaoPerfil from './Pages/EdicaoPerfil/EdicaoPerfil'
import Turmas from './Pages/Turmas/Turmas'
import Historico from './Pages/Historico/Historico'
import InscricaoMaterias from './Pages/InscricaoMaterias/InscricaoMaterias'
import Periodo_Letivo from './Pages/PeriodoLetivo/PeriodoLetivo'
import Departamento from './Pages/Departamento/Departamento'
import MateriaAdicionar from './Pages/MateriaAdicionar/MateriaAdicionar'
import Professores from './Pages/ProfessoresEdicao/ProfessoresEdicao'
import ProfessoresLista from './Pages/Professores/Professores'
import Curso from './Pages/Curso/Curso'
import Coordenadores from './Pages/Coordenadores/Coordenadores'
import CoordenadoresEdicao from './Pages/CoordenadoresEdicao/CoordenadoresEdicao'
import Alunos from './Pages/Alunos/Alunos'
import AlunosEdicao from './Pages/AlunosEdicao/AlunosEdicao'
import { Context } from './Context/Context'
import createHistory from 'history/createBrowserHistory'
import TurmasGerencia from './Pages/TurmasGerencia/TurmasGerencia'
import TurmaTotal from './Pages/TurmaTotal/TurmaTotal'
import TurmaTotalAdd from './Pages/TurmaTotalAdd/TurmaTotalAdd'
import Materias from './Pages/Materias/Materias'


function CustomRoute({ isPrivate, ...rest }){
    const {loading, authenticated} = useContext(Context)
    
    if (loading) {
        return <Redirect to="/login" />
    }

    if (isPrivate && !authenticated) {
        return <Redirect to="/Login"/>}

        return<route {...rest} />
}

export default function Routes() { 
    const history = createHistory();  
    let userLog = JSON.parse(localStorage.getItem('userLog'))
    if (userLog === null)
        userLog = {user:{
            "role":"dummy"}} //fiz essa gambiarra porque caso o usuario nao esteja 
                             //logado, o react crasha dizendo "cannot read roll propety
                             //of 'null' ", ai pus um dummy para ele não crashar enquanto
                             //o usuário não loga 
    return(
        <>
        <Router history={history}>
            <Switch>
                 <CustomRoute exact path="/">
                    <Redirect to="/Login"></Redirect>
                </CustomRoute> 
                <CustomRoute exact path="/Login">
                    <Login/>
                </CustomRoute>
                <CustomRoute isPrivate exact path="/curriculos">
                    <Curriculos/>
                </CustomRoute>
                <CustomRoute isPrivate exact path="/perfil">
                    <EdicaoPerfil/>
                </CustomRoute>
                <CustomRoute isPrivate exact path="/turmas">
                {(userLog.role === 'student' || userLog.role === 'teacher') && <Turmas/>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/historico">
                {(userLog.role === 'student') && <><Historico/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/inscricao">
                {(userLog.role === 'student') && <><InscricaoMaterias/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/departamento">
                {(userLog.role === 'principal' || userLog.role === 'departmentcoordinator') && <><Departamento/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/periodoletivo">
                {(userLog.role === 'departmentcoordinator') && <><Periodo_Letivo/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/adicionar_materia">
                {(userLog.role === 'departmentcoordinator') && <><MateriaAdicionar/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/professores">
                {(userLog.role === 'departmentcoordinator') && <><Professores/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/professoreslista">
                {(userLog.role === 'departmentcoordinator') && <><ProfessoresLista/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/curso">
                {(userLog.role === 'coursecoordinator' || userLog.role === 'principal') && <><Curso/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/coordenadores">
                {(userLog.role === 'principal') && <><Coordenadores/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/Coordenadoresedicao">
                {(userLog.role === 'principal') && <><CoordenadoresEdicao/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/alunos">
                {(userLog.role === 'coursecoordinator') && <><Alunos/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/alunosedicao">
                {(userLog.role === 'coursecoordinator') && <><AlunosEdicao/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/TurmasGerencia">
                {(userLog.role === 'teacher') && <><TurmasGerencia/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/TurmaTotal">
                {(userLog.role === 'departmentcoordinator') && <><TurmaTotal/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/TurmaTotalAdd">
                {(userLog.role === 'departmentcoordinator') && <><TurmaTotalAdd/></>}
                </CustomRoute>
                <CustomRoute isPrivate exact path="/Materias">
                {(userLog.role === 'departmentcoordinator') && <><Materias/></>}
                </CustomRoute>
                
            </Switch>        
        </Router>
        </>
    )
}